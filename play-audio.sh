#!/usr/bin/env sh

if [ -f "$1" ] && [ -n "$(printf "%b\n" "$1" | sed -r "s|\\.[^\\.]+$||")" ]; then
    media="$1"
else
    formats="$AUDIO_FORMATS"
    formats="$(printf "%b\n" "$formats" | sed 's/,/\\|/g')"

    regex=".*\\.\($formats\)"

    trash="\($(printf "%b\n" "$TRASH" | sed 's/:/\/.*\\|/')/.*\)"

    multimedia="$(find "$HOME" -regex "$regex" -not -regex "$trash")"
    multimedia="$(printf "%b\n" "$multimedia" | sed "s|^$HOME/||")"

    multimedia="${multimedia}$(find "$DEVICES_FOLDER" -regex "$regex" \
        -not -regex "$trash")"

    media="$(printf "%b\n" "${multimedia}" | dmenu -i -l 9)"

    [ "$(printf "%b\n" "$media" | grep -E "$DEVICES_FOLDER/.*" | wc -l)" -eq 0 ] &&
        media="$HOME/$media"
fi

[ "$media" != "$HOME/" ] && {
    if [ "$(command -v disown > /dev/null)" ]; then
        st -c "Music Player" -e "$AUDIOPLAYER" "$media" 2>/dev/null & disown
    else
        st -c "Music Player" -e "$AUDIOPLAYER" "$media" 2>/dev/null &
    fi
}
