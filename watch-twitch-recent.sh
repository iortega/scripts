#!/usr/bin/env sh

watch=$(dmenu -l 15 -c -i -fn "$DMENUFN" -p "Streamer name" \
	< "$HOME/.local/share/watch-twitch.streams" -B 5)

[ -n "$watch" ] && {
    dash $TSCRIPTS/watch-video-select-format.sh "https://www.twitch.tv/$watch"
}
