#!/usr/bin/env sh

script=$(find -L "$SCRIPTS/" -type f -name "*.sh" | sed "s|^$SCRIPTS/||" |
    dmenu -fn "$DMENUFN")
script="$SCRIPTS/$script"
dash $script
