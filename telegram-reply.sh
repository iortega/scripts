#!/usr/bin/env sh

$TELEGRAM

iterations=100

i=0
while [ "$i" -lt $iterations ]
do
    window_id=$(xdotool search --onlyvisible --class Telegram)

    if [ -n "$window_id" ]; then
        correct=$(xprop -id "$window_id" | grep "window state: Normal")
    fi

    [ -n "$correct" ] && break || i=$(( $i + 1 ))
    sleep 0.2
done

if [ "$i" -lt $iterations ]; then
    sleep 0.6
    xdotool key super+7 && sleep 0.5 && xdotool key Return
fi
