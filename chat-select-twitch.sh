#!/usr/bin/env bash

watch=$(cat $FAV_STREAMERS | dmenu -l 15 -c -i -fn "$DMENUFN" \
	-p "Streamer:" -B 5)

[ -n "$watch" ] && $BROWSER "https://www.twitch.tv/popout/$watch/chat?popout="
