#!/usr/bin/env sh

colorpicker --short --one-shot --preview | xclip -selection clipboard
