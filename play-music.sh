#!/usr/bin/env sh

dash $SCRIPTS/musicplayerui.sh
case "$MUSICPLAYER" in
        "cmus-remote")

                dash -c "cmus-remote -C 'view tree'"
                dash -c "cmus-remote -C clear"
                dash -c "cmus-remote -C \"view queue\""
                dash -c "cmus-remote -C clear"
                sleep 1
                dash -c "cmus-remote -C \"view sorted\""
                dash -c "cmus-remote -C clear"
                dash -c "cmus-remote -C \"add $MUSIC\""
                sleep 1
                dash -c "cmus-remote -C player-next"
                dash -c "cmus-remote -C player-play"
                ;;

        "mpc")
                mpc || mpd
                mpc clear
                mpc update
                mpc ls | mpc add
                mpc shuffle
                mpc play
                ;;
        "mocp")
                while ! mocp -o shuffle 2>/dev/null; do
                        sleep 1
                done
                mocp -c
                sleep 1
                mocp -a ~/Music/music
                mocp -p
                ;;
esac
