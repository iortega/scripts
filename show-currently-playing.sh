#!/usr/bin/env sh

nothing_notice='Nothing is being played'
if $MUSICPLAYERSTATUS 2>/dev/null; then
    status=$($MUSICPLAYERSTATUS)
	case "$MUSICPLAYER" in
		"mocp")
			if [ "$status" = "State: STOP" ]; then
				notify-send "$nothing_notice"
			else
				mocp -i | sed '2q;d' | cut -d' ' -f 2- | 
					xargs -I{} basename '{}' |
					xargs -I{} notify-send 'Now Playing:' '{}'
			fi
			;;
		"mpc")
				notify-send "Not prepared"
			;;
	esac
fi
