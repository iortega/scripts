#!/usr/bin/env sh

if [ -z "$1" ]; then
    link="$(dash $SCRIPTS/yt-search.sh)"
else
    link="$1"
fi

confirm=$(printf "%b\n" "Yes\nNo" | dmenu -c -i -fn "monospace:size=20" -p \
"Confirm:" -B 5)

[ "$confirm" = "Yes" ] && dash $SCRIPTS/download-audio.sh -d $MUSIC $link ||
    notify-send "Download cancelled"
