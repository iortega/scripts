#!/usr/bin/env sh

if [ -z "$1" ]; then
    link="$(dash $SCRIPTS/yt-search.sh)"
else
    link="$1"
fi

confirm=$(printf "%b\n" "Yes\nNo" | dmenu -l 2 -c -i -fn "$DMENUFN" \
	-p "Confirm:")

[ "$confirm" = "Yes" ] && dash $SCRIPTS/download-audio.sh $link ||
    notify-send "Download cancelled"
