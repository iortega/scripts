#!/usr/bin/env sh

max() {
	echo "$(( $2 > $1 ? $2 : $1 ))"
}

min() {
	echo "$(( $2 < $1 ? $2 : $1 ))"
}

general_change=6

if [ "$1" = "up" ]; then
    # amixer -q sset Master 5%+
	vol=$(aucatctl master | cut -f 2 -d '=')
	change=$(min "$(( 127 - $vol ))" "$general_change")
	new="$vol"
	while [ "$vol" = "$new" ] && [ "$vol" -lt "127" ]; do
		aucatctl master=$(( $vol + $change ))
		new=$(aucatctl master | cut -f 2 -d '=')
	done
	dash $SCRIPTS/reset-dwmbar.sh && dash $SCRIPTS/volume-notification.sh
else
	# amixer -q sset Master 5%-
	vol=$(aucatctl master | cut -f 2 -d '=')
	change=$(min "$vol" "$general_change")
	new="$vol"
	while [ "$vol" = "$new" ] && [ "$vol" -gt "0" ]; do
		aucatctl master=$(( $vol - $change ))
		new=$(aucatctl master | cut -f 2 -d '=')
	done
	dash $SCRIPTS/reset-dwmbar.sh && dash $SCRIPTS/volume-notification.sh
fi
