#!/usr/bin/env sh
# xscreensaverstopper.sh

# This script is licensed under GNU GPL version 2.0 or above

# Uses elements from lightsOn.sh
# Copyright (c) 2011 iye.cba at gmail com
# url: https://github.com/iye/lightsOn
# This script is licensed under GNU GPL version 2.0 or above

# Description: Restarts xscreensaver's idle countdown while 
# full screen applications are running.  
# Checks every 30 seconds to see if a full screen application
# has focus, if so then the xscreensaver is told to restart 
# its idle countdown.


# enumerate all the attached screens
tmp_file="/tmp/xscreensaverstopper$(date +%s)"
xvinfo | sed -n 's/^screen #\([0-9]\+\)$/\1/p' > $tmp_file
displays=""
while read id
do
    displays="$displays $id"
done< $tmp_file

checkFullscreen()
{

    # loop through every display looking for a fullscreen window
    for display in $displays
    do
        #get id of active window and clean output
        activ_win_id=`DISPLAY=:0.${display} xprop -root _NET_ACTIVE_WINDOW | \
            cut -c 41-49`
        # activ_win_id="$(printf "%b\n" "$activ_win_id" | cut -c 41-49 )"
        
        # Check if Active Window (the foremost window) is in fullscreen state
        [ -n "$activ_win_id" ] && isActivWinFullscreen=`DISPLAY=:0.${display} \
            xprop -id $activ_win_id | grep _NET_WM_STATE_FULLSCREEN`
        if [ -n "$isActivWinFullscreen" -a -z "${isActivWinFullscreen##*NET_WM_STATE_FULLSCREEN*}" ]; then
        	xscreensaver-command -deactivate
        	# xscreensaver-command -exit
        	# xscreensaver-command -restart
            # xdotool key Caps_Lock && xdotool key Caps_Lock
        # else
            # xscreensaver -no-splash &
	    fi
    done
}

while sleep $(( 30 )); do
    checkFullscreen
done

exit 0
