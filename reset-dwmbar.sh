#!/usr/bin/env sh

resetlockfile="/tmp/dwmbar-reset-lock"
[ ! -f "$resetlockfile" ] && {
    touch "$resetlockfile"
    p_num=$(cat /tmp/dwmbar_pid)
    [ -n "$p_num" ] && kill -s 5 "$p_num"
    rm "$resetlockfile"
}
