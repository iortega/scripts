#!/usr/bin/env sh

watch=$(dmenu -l 15 -c -i -fn "$DMENUFN" -p "Streamer:" -B 5 \
	< $FAV_STREAMERS)

[ -n "$watch" ] && {
    if [ -n "$(youtube-dl --get-id "https://www.twitch.tv/$watch")" ]; then
        dash $TSCRIPTS/watch-video-select-format.sh \
            "https://www.twitch.tv/$watch"
    else
        notify-send "Not Found Streamer:" "$watch"
    fi
}
