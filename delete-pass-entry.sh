#!/usr/bin/env sh

site=$(find "$PASSWORD_STORE_DIR" -name "*.gpg" -type f | \
    sed "s|^$PASSWORD_STORE_DIR/||" | sed "s|\\.gpg$||" | dmenu -l 15 -c -i \
	-fn "$DMENUFN" -p "Entry" -B 5 -B 5)

[ -z "$site" ] && exit 0

pass remove "$site"
