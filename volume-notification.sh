#!/usr/bin/env sh

# Arbitrary but unique message id
msgId="991049"

# level=$(amixer get Master | grep -o "[0-9]*%\|\[on\]\|\[off\]" |
#     sed "s/\[on\]//;s/\[off\]//" | head -n 2)

# bar=$(echo "$level" | head -n 1 | tr '%' ' ' |
#           xargs dash "$SCRIPTS/progress-bar.sh")

# echo "$level" | xargs echo |
# 	xargs -I{} dunstify -a "changeVolume" -t 3000 -u low -i audio-volume-high -r "$msgId" \
#     "Volume: {}" "$bar"

vol=$(aucatctl master | cut -f 2 -d '=')
vol100scale=$(echo "$vol * 100 / 127" | bc)

bar=$(echo "$vol100scale" | xargs dash "$SCRIPTS/progress-bar.sh")


echo "$vol100scale%" |
	xargs -I{} dunstify -a "changeVolume" -t 3000 -u low -i audio-volume-high -r "$msgId" \
    "Volume: {}" "$bar"
